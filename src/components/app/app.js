import React, {Component} from "react";
import AppHeader from "../app-header";
import SearchPanel from "../search-panel";
import ItemStatusFilter from "../item-status-filter";
import TodoList from "../todo-list";
import ItemAddForm from "../item-add-form";

import "./app.css"

export default class App extends Component {
    maxId = 100;

    state = {
        todoData: [
            this.createTodoItem("DrinkCoffee"),
            this.createTodoItem("MakeApp"),
            this.createTodoItem("Make a lunch")
        ]
    }
    deleteItem = (id) => {
        this.setState(({todoData})=> {
            const idx = todoData.findIndex((el)=> el.id === id)
            // todoData.splice(idx, 1) - нельзя, т.к. меняем существующий стейт, что вредно. копируем и возвращаем
            //Неизменность стейта!
            const before = todoData.slice(0, idx)
            const after = todoData.slice(idx + 1)
            const newArray = [...before, ...after]
            // const newArr = [...todoData.slice(0, idx), ...todoData.slice(idx + 1)]
            return {
                todoData: newArray
            }
        })
    };

    createTodoItem (label) {
        return {
            label,
            important: false,
            done: false,
            id: this.maxId++
        };
    };

    addItem = (text) => {
        const newItem = this.createTodoItem(text);
        this.setState(({todoData})=> {
            const newArr = [...todoData, newItem]
            return {
                todoData: newArr
            }
        })
    }
    onToggleDone = (id) => {
        this.setState(({todoData}) => {
            return {
                todoData: this.toggleProperty(todoData, id, "done"),
            };
        });
    };

    onToggleImportant = (id) => {
        this.setState(({todoData}) => {
            return {
                todoData: this.toggleProperty(todoData, id, "important"),
            };
        });
    };

    toggleProperty(arr, id, propName) {
        const idx = arr.findIndex((el)=> el.id === id)
        //create & update obj
        const oldItem = arr[idx]
        const newItem = {...oldItem,
            [propName]: !oldItem[propName]}
        //create new
        return [
            ...arr.slice(0, idx),
            newItem,
            ...arr.slice(idx + 1)
        ];
    }

    render() {
        const {todoData} = this.state;
        const doneCount = todoData.filter((el) => el.done).length;
        const todoCount = todoData.length - doneCount;

        return (
            <div className="todo-app">
                <AppHeader toDo={todoCount} done={doneCount}/>
                <div className="top-panel d-flex">
                    <SearchPanel/>
                    <ItemStatusFilter/>
                </div>
                <TodoList todos={todoData}
                          onDeleted={this.deleteItem}
                          onToggleImportant={this.onToggleImportant}
                          onToggleDone={this.onToggleDone}
                />
                <ItemAddForm onItemAdded={this.addItem}/>
            </div>
        );
    }
};
